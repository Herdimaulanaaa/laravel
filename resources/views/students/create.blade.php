@extends('layout.main')

@section('title','Form Tambah Data Mahasiswa')

@section('container')

     <div class="container">
      <div class="row">
        <div class="col-8">
          <h1 class="mt-3">Form Tambah Data Mahasiswa</h1>
          

          @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
          
          <form method="post" action="/students" >
            {!!csrf_field()!!}
              

             <div class="form-group">

                <label for="nama">Nama</label>
                <input type="text" class="form-control" id="nama" placeholder="masukan nama" name="nama" value="{{old('nama')}}">

             </div>

             <div class="form-group">

                <label for="nrp">NRP</label>
                <input type="text" class="form-control" id="nrp" placeholder="masukan nrp" name="nrp" value="{{old('nrp')}}">

             </div>  

             <div class="form-group">

                <label for="email">Email</label>
                <input type="text" class="form-control" id="email" placeholder="masukan email" name="email" value="{{old('email')}}">

             </div>

            <div class="form-group">

                <label for="jurusan">Jurusan</label>
                <input type="text" class="form-control" id="jurusan" placeholder="masukan jurusan" name="jurusan" value="{{old('jurusan')}}">

             </div>

             <button type="submit" class="btn btn-primary">Tambah Data!!</button>
          
        </form>


        </div>
      </div>
     </div> 
@endsection
