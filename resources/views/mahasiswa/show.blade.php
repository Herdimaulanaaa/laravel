@extends('layout.main')

@section('title','Daftar Mahasiswa')

@section('container')

     <div class="container">
      <div class="row">
        <div class="col-10">
          <h1 class="mt-3">Daftar Siswa</h1>

          <table class="table">
          	<thead class="thead-dark">
          		<tr>
	          		<th scope="col">#</th>
	          		<th scope="col">Nama</th>
	          		<th scope="col">Nrp</th>
	          		<th scope="col">E-mail</th>
	          		<th scope="col">Jurusan</th>
	          		<th scope="col">Aksi</th>
            	</tr>
       		</thead>
          	<tbody>

          		@foreach ($mahasiswa as $mhs)

          		<tr>
          			<th scope="row">{{ $loop->iteration}}</th>
          			<td>{{ $mhs->nama }}</td>
          			<td>{{ $mhs->nrp }}</td>
          			<td>{{ $mhs->email }}</td>
          			<td>{{ $mhs->jurusan }}</td>
          			<td>
          				<a href="{{$student->id}}/edit" class="badge badge-success">edit</a>
                  <form action="{{$mahasiswa->id}}" method="post">
                    {!!method_field('delete')!!}
                    {!!scrf_field()!!}              
          				  <button type="submit" class="btn btn-danger">delete</button>
                  </form>            
          			</td>
          		</tr>
          		@endforeach
          	</tbody>

          </table>

            <a href="/mahasiswa/create" class="btn btn-primary " style="margin-left: 700px" >Tambah Data Mahasiswa</a>

              @if (session('status'))
            <div class="alert alert-success">
               {{session('status')}}
            </div>
            @endif
        </div>
      </div>
     </div> 
@endsection
