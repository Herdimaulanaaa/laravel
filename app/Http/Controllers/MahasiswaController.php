<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\mahasiswa;

class MahasiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$mahasiswa = DB::table('students')->get();
        $mahasiswa = mahasiswa::all();
     
        return view ('mahasiswa.index',['mahasiswa' => $mahasiswa]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('mahasiswa.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /* $mahasiswa = new mahasiswa;
        $mahasiswa->nama =$request->nama;
        $mahasiswa->nrp =$request->nrp;
        $mahasiswa->email =$request->email;
        $mahasiswa->jurusan =$request->jurusan;

        $mahasiswa->save();*/

       /* Mahasiswa::create([
            'nama'=>$request->nama,
            'nrp'=>$request->nrp,
            'email'=>$request->email,
            'jurusan'=>$request->jurusan,
        ]);
            */

          $this->validate($request, [

           'nama' =>'required',
           'nrp' =>'required',
           'email' =>'required',
           'jurusan' =>'required'
       

       ]);
        Mahasiswa::create($request->all());
        return redirect('/mahasiswa')->with ('status','Data Mahasiswa Berhasil Ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
           return view('mahasiswa,show',compact('mahasiswa'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         return view('mahasiswa.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return view($mahasiswa);
    }
}
