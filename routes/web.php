<?php


Route::get('/','PagesController@home');
Route::get('/about','PagesController@about');

Route::get('/mahasiswa','MahasiswaController@index');


//Students
Route::get('/students','StudentsController@index');
Route::get('/students/create','StudentsController@create');
Route::get('/students/{student}','StudentsController@show');
Route::post('/students','StudentsController@store');
Route::delete('/students/{student}','StudentsController@destroy');
Route::get('students/{student}/edit','StudentsController@edit');
Route::patch('/students/{student}','StudentsController@update');

//mahasiswa
Route::get('/mahasiswa/create','MahasiswaController@create');
Route::get('/mahasiswa/{mahasiswa}','MahasiswaController@show');
Route::post('/mahasiswa','MahasiswaController@store');
Route::delete('/mahasiswa/{mahasiswa}','MahasiswaController@destroy');

//Route::resource('students','StudentsController');
//Route::resource('mahasiswa','MahasiswaController');






?>




